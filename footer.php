<?php
/**
 * Footer template.
 *
 * @package semiphoton
 * @since 1.0.0
 *
 */

$footer_logo = get_field('footer_logo', 'option');
$footer_address = get_field('footer_address', 'option');
$footer_info = get_field('footer_info', 'option');
$youtube_url = get_field('youtube_url', 'option');

$copyright_text = get_field('copyright_text', 'option');
?>

</main>

<footer class="sem-footer">
    <div class="container">
        <div class="d-flex sem-footer__wrap">
            <div class="sem-footer__col col_main">
                <a href="<?php echo home_url('/'); ?>" class="sem-footer__logo">
                    <img src="<?php echo esc_url( $footer_logo['url'] ); ?>" alt="logo">
                </a>

                <?php if ( ! empty( $footer_address ) ) : ?>
                    <div class="sem-footer__text d-none d-lg-block"><?php echo wpautop( $footer_address ); ?></div>
                <?php endif; ?>

                <?php if ( ! empty( $youtube_url ) ) : ?>
                    <div class="sem-soc d-none d-lg-flex">
                        <a href="<?php echo esc_url( $youtube_url ); ?>" class="sem-soc__item" target="_blank">
                            <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M12.5 18.75L18.9875 15L12.5 11.25V18.75ZM26.95 8.9625C27.1125 9.55 27.225 10.3375 27.3 11.3375C27.3875 12.3375 27.425 13.2 27.425 13.95L27.5 15C27.5 17.7375 27.3 19.75 26.95 21.0375C26.6375 22.1625 25.9125 22.8875 24.7875 23.2C24.2 23.3625 23.125 23.475 21.475 23.55C19.85 23.6375 18.3625 23.675 16.9875 23.675L15 23.75C9.7625 23.75 6.5 23.55 5.2125 23.2C4.0875 22.8875 3.3625 22.1625 3.05 21.0375C2.8875 20.45 2.775 19.6625 2.7 18.6625C2.6125 17.6625 2.575 16.8 2.575 16.05L2.5 15C2.5 12.2625 2.7 10.25 3.05 8.9625C3.3625 7.8375 4.0875 7.1125 5.2125 6.8C5.8 6.6375 6.875 6.525 8.525 6.45C10.15 6.3625 11.6375 6.325 13.0125 6.325L15 6.25C20.2375 6.25 23.5 6.45 24.7875 6.8C25.9125 7.1125 26.6375 7.8375 26.95 8.9625Z" fill="#F8C323" />
                            </svg>
                        </a>
                    </div>
                <?php endif; ?>
            </div>

            <div class="sem-footer__col col_nav">
                <h5 class="sem-footer__title"><?php esc_html_e('Solar + PV', SP_PREFIX); ?></h5>
                <?php wp_nav_menu(
                    array(
                        'container' => '',
                        'items_wrap' => '<ul class="sem-footer__menu">%3$s</ul>',
                        'theme_location' => 'footer-menu-1',
                        'depth' => 1,
                        'fallback_cb' => '__return_empty_string',
                    )
                ); ?>
            </div>

            <div class="sem-footer__col col_nav">
                <h5 class="sem-footer__title"><?php esc_html_e('Sorting Lines', SP_PREFIX); ?></h5>
                <?php wp_nav_menu(
                    array(
                        'container' => '',
                        'items_wrap' => '<ul class="sem-footer__menu">%3$s</ul>',
                        'theme_location' => 'footer-menu-2',
                        'depth' => 1,
                        'fallback_cb' => '__return_empty_string',
                    )
                ); ?>
            </div>

            <div class="sem-footer__col col_nav">
                <h5 class="sem-footer__title"><?php esc_html_e('Semiconductor', SP_PREFIX); ?></h5>
                <?php wp_nav_menu(
                    array(
                        'container' => '',
                        'items_wrap' => '<ul class="sem-footer__menu">%3$s</ul>',
                        'theme_location' => 'footer-menu-3',
                        'depth' => 1,
                        'fallback_cb' => '__return_empty_string',
                    )
                ); ?>
            </div>

            <div class="sem-footer__col col_contact">
                <?php semiphoton_btn($footer_info['link'], 'sem-btn sem-btn_border mb-15'); ?>

                <?php if ( ! empty( $footer_address ) ) : ?>
                    <div class="sem-contact-line d-flex d-lg-none text_white mb-15">
						<span class="icon">
							<svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18" fill="none">
								<path d="M9 1.5C6.0975 1.5 3.75 3.8475 3.75 6.75C3.75 10.6875 9 16.5 9 16.5C9 16.5 14.25 10.6875 14.25 6.75C14.25 3.8475 11.9025 1.5 9 1.5ZM9 8.625C8.50272 8.625 8.02581 8.42745 7.67418 8.07582C7.32254 7.72419 7.125 7.24728 7.125 6.75C7.125 6.25272 7.32254 5.7758 7.67418 5.42417C8.02581 5.07254 8.50272 4.875 9 4.875C9.49728 4.875 9.9742 5.07254 10.3258 5.42417C10.6775 5.7758 10.875 6.25272 10.875 6.75C10.875 7.24728 10.6775 7.72419 10.3258 8.07582C9.9742 8.42745 9.49728 8.625 9 8.625Z" fill="#F8C323" />
							</svg>
						</span>
                        <?php echo wpautop( $footer_address ); ?>
                    </div>
                <?php endif; ?>

                <?php if ( ! empty( $footer_info['email'] ) ) : ?>
                    <a href="mailto:<?php echo $footer_info['email']; ?>" class="sem-contact-line text_white mb-15">
                        <span class="icon">
                            <svg width="18" height="19" viewBox="0 0 18 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M1.45913 7.24031C1.06763 7.10981 1.06388 6.89906 1.46663 6.76481L15.7819 1.99331C16.1786 1.86131 16.4059 2.08331 16.2949 2.47181L12.2044 16.7863C12.0919 17.1831 11.8631 17.1966 11.6951 16.8201L8.99963 10.7541L13.4996 4.75406L7.49963 9.25406L1.45913 7.24031Z" fill="#F8C323" />
                            </svg>
                        </span>
                        <?php echo $footer_info['email']; ?>
                    </a>
                <?php endif; ?>

                <?php if ( ! empty( $footer_info['phone'] ) ) : ?>
                    <a href="<?php echo $footer_info['phone']; ?>" class="sem-contact-line text_white mb-15 mb-lg-0">
                        <span class="icon">
                            <svg width="18" height="19" viewBox="0 0 18 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M4.965 8.34641C6.045 10.4689 7.785 12.2014 9.9075 13.2889L11.5575 11.6389C11.76 11.4364 12.06 11.3689 12.3225 11.4589C13.1625 11.7364 14.07 11.8864 15 11.8864C15.4125 11.8864 15.75 12.2239 15.75 12.6364V15.2539C15.75 15.6664 15.4125 16.0039 15 16.0039C7.9575 16.0039 2.25 10.2964 2.25 3.25391C2.25 2.84141 2.5875 2.50391 3 2.50391H5.625C6.0375 2.50391 6.375 2.84141 6.375 3.25391C6.375 4.19141 6.525 5.09141 6.8025 5.93141C6.885 6.19391 6.825 6.48641 6.615 6.69641L4.965 8.34641Z" fill="#F8C323" />
                            </svg>
                        </span>
                        <?php echo $footer_info['phone']; ?>
                    </a>
                <?php endif; ?>

                <?php if ( ! empty( $youtube_url ) ) : ?>
                    <div class="sem-soc d-flex d-lg-none">
                        <a href="<?php echo esc_url( $youtube_url ); ?>" class="sem-soc__item">
                            <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M12.5 18.75L18.9875 15L12.5 11.25V18.75ZM26.95 8.9625C27.1125 9.55 27.225 10.3375 27.3 11.3375C27.3875 12.3375 27.425 13.2 27.425 13.95L27.5 15C27.5 17.7375 27.3 19.75 26.95 21.0375C26.6375 22.1625 25.9125 22.8875 24.7875 23.2C24.2 23.3625 23.125 23.475 21.475 23.55C19.85 23.6375 18.3625 23.675 16.9875 23.675L15 23.75C9.7625 23.75 6.5 23.55 5.2125 23.2C4.0875 22.8875 3.3625 22.1625 3.05 21.0375C2.8875 20.45 2.775 19.6625 2.7 18.6625C2.6125 17.6625 2.575 16.8 2.575 16.05L2.5 15C2.5 12.2625 2.7 10.25 3.05 8.9625C3.3625 7.8375 4.0875 7.1125 5.2125 6.8C5.8 6.6375 6.875 6.525 8.525 6.45C10.15 6.3625 11.6375 6.325 13.0125 6.325L15 6.25C20.2375 6.25 23.5 6.45 24.7875 6.8C25.9125 7.1125 26.6375 7.8375 26.95 8.9625Z" fill="#F8C323" />
                            </svg>
                        </a>
                    </div>
                <?php endif; ?>
            </div>
            
            <?php if ( ! empty( $copyright_text ) && 1 ): ?>
                <div class="sem-footer__copy">
                    <?php echo $copyright_text; ?>
                </div>
            <?php endif ?>
        </div>
    </div>
</footer>

<script>window.gtranslateSettings = {"default_language":"en","native_language_names":true,"detect_browser_language":true,"languages":["en","es","fr","zh-CN"],"wrapper_selector":".gtranslate_wrapper","flag_size":16,"switcher_horizontal_position":"inline","alt_flags":{"en":"usa"}}</script>
<script src="https://cdn.gtranslate.net/widgets/v1.0.1/dwf.js" defer></script>

	<?php wp_footer(); ?>
	</body>
</html>
