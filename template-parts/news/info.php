<?php
$news_info = get_field('news_info', 'option');

if ( ! empty( $news_info ) ) : ?>
    <div class="sem-connection mb-lg-150 mb-md-100 mb-50">
        <div class="container">
            <div class="sem-connection__content">
                <h3><?php echo wp_kses_post( $news_info['text'] ); ?></h3>
                <?php semiphoton_btn($news_info['link'], 'sem-btn sem-btn_main'); ?>
            </div>
        </div>
    </div>
<?php endif;
