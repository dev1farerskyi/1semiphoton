<?php
/**
 *  Content And List Block
 */

$style = get_sub_field('style');
$title = get_sub_field('title');
$subtitle = get_sub_field('subtitle');
$text = get_sub_field('text');
$link = get_sub_field('link');
$image = get_sub_field('image');
$list = get_sub_field('list');

$classes = array();
$classes[] = 'style-' . $style;
$classes[] = 'col-' . get_sub_field('columns_count');
?>

<div id="sem-block-<?php echo get_row_index(); ?>" class="sem-customers design_customers <?php echo implode(' ', $classes); ?>">
    <div class="container">
        <div class="<?php echo ! empty( $image ) ? 'd-flex' : ''; ?> sem-customers__wrap">
            <?php if ( ! empty( $image ) ): ?>
                <div class="sem-customers__image">
                    <img src="<?php echo esc_url($image['url']); ?>" alt="image">
                </div>
            <?php endif; ?>

            <div class="sem-customers__content">
                <?php if ( $style !== 's2' ) : ?>
                    <?php if ( ! empty( $subtitle ) ): ?>
                        <h5 class="sem-customers__subtitle">
                            <?php echo wp_kses_post($subtitle); ?>
                        </h5>
                    <?php endif; ?>

                    <?php if ( ! empty( $title ) ): ?>
                        <h3 class="sem-customers__title">
                            <?php echo wp_kses_post($title); ?>
                        </h3>
                    <?php endif; ?>
                <?php endif; ?>

                <?php if ( ! empty( $text ) ): ?>
                    <div class="sem-customers__text">
                        <?php echo wpautop($text); ?>
                    </div>
                <?php endif; ?>

                <?php if ( ! empty( $list ) ): ?>
                    <div class="d-none d-lg-block sem-customers__info">
                        <?php echo wpautop($list); ?>
                    </div>
                <?php endif; ?>

                <?php if ( $style === 's2' ) : ?>
                    <?php if ( ! empty( $subtitle ) ): ?>
                        <h5 class="sem-customers__subtitle">
                            <?php echo wp_kses_post($subtitle); ?>
                        </h5>
                    <?php endif; ?>
                <?php endif; ?>

                <?php semiphoton_btn($link, 'sem-btn sem-btn_main'); ?>
            </div>
        </div>
    </div>
</div>
