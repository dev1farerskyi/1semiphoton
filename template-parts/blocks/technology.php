<?php
/**
 *  Technology Block
 */

$link = get_sub_field('link');
$technology_content = get_sub_field('technology_content');
?>

<div id="sem-block-<?php echo get_row_index(); ?>" class="sem-technology">
    <div class="container">
        <div class="row justify-content-center">
            <?php foreach ($technology_content as $row): ?>
                <div class="col-xl-4 col-sm-6">
                    <div class="sem-technology__item">
                        <div class="sem-technology__item-image">
                            <img src="<?php echo esc_url($row['image']['url']); ?>" alt="image">
                        </div>
                        <h2 class="sem-technology__item-title"><?php echo wp_kses_post($row['title']); ?></h2>
                        <div class="sem-technology__item-text">
                            <?php echo wpautop($row['text']); ?>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="d-flex justify-content-center">
            <?php semiphoton_btn($link, 'sem-btn sem-btn_main'); ?>
        </div>
    </div>
</div>