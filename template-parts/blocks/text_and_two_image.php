<?php
/**
 * Text And Two Images Block
 */
$images = get_sub_field('gallery_images');
$title = get_sub_field('title');
$text = get_sub_field('text');
$button = get_sub_field('button');
?>

<div id="sem-block-<?php echo get_row_index(); ?>" class="sem-card-info card_grad">
    <div class="sem-card-info__wrap">
        <div class="container">
            <div class="row align-items-center @@position">
                <div class="col-xl-4 col-lg-6">
                    <div class="sem-card-info__content">
                        <?php if ( ! empty( $title ) ): ?>
                            <h4 class="sem-card-info__title"><?php echo wp_kses_post($title); ?></h4>
                        <?php endif; ?>
                        <?php if ( ! empty( $text ) ): ?>
                            <div class="sem-card-info__text">
                                <?php echo wpautop($text); ?>

                                <?php semiphoton_btn($button, 'sem-btn sem-btn_border mt-20 mb-15'); ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="col-xl-8 col-lg-6">
                    <div class="row">
                        <?php if ($images): ?>
                            <div class="sem-card-info__wrap sem-card-info__wrap_wrap">
                                <?php foreach ($images as $image): ?>
                                    <div class="col-lr-3">
                                        <div class="sem-card-info__image">
                                            <img src="<?php echo $image['sizes']['large']; ?>" alt="image">
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>