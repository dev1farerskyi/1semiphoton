<?php
/**
 * Hero Block
 */

$media = get_sub_field('media');
$image = get_sub_field('image');
$video_url = get_sub_field('video_url', false);
?>

<div id="sem-block-<?php echo get_row_index(); ?>" class="sem-hero">
    <div class="container">
        <div class="sem-hero__<?php echo esc_attr( $media ); ?>">
            <?php if ( $media === 'video' ) : ?>
                <div data-vbg="<?php echo $video_url; ?>"></div>
            <?php else : ?>
                <img src="<?php echo esc_url( $image['url'] ); ?>" alt="image">
            <?php endif; ?>
        </div>
    </div>
</div>
