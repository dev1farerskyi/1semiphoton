<?php
/**
 *  Sorting Slider Block
 */

$subtitle = get_sub_field('subtitle');
$text = get_sub_field('text');
$slider = get_sub_field('slider');
$extra_top_space = get_sub_field('extra_top_space');

$text_class    = ! empty( $subtitle ) ? 'col-lg-6' : 'col-lg-12';
$block_classes = array('sem-about');

if ( $extra_top_space ) {
    $block_classes[] = 'extra-space';
}
?>

<div id="sem-block-<?php echo get_row_index(); ?>" class="<?php echo implode(' ', $block_classes); ?>">
    <div class="container">
        <div class="sem-about__content">
            <div class="row">
                <div class="col-lg-6">
                    <?php if ( ! empty( $subtitle ) ): ?>
                        <h3 class="sem-about__subtitle"><?php echo wp_kses_post($subtitle); ?></h3>
                    <?php endif; ?>
                </div>
                <div class="<?php echo $text_class; ?>">
                    <?php if ( ! empty( $text ) ): ?>
                        <div class="sem-about__text"><?php echo wpautop($text); ?></div>
                    <?php endif; ?>
                </div>
            </div>
        </div>

        <?php if ( ! empty( $slider ) ) : ?>
            <div class="sem-about__slider">
                <div class="swiper sem-image-slider">
                    <div class="swiper-wrapper">
                        <?php foreach ($slider as $row): ?>
                            <?php $image = $row['image']; ?>
                            <div class="swiper-slide">
                                <div class="sem-about__slider-item">
                                    <img src="<?php echo esc_url($image['url']); ?>" alt="image">
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
                <div class="swiper-button-prev sem-image-slider-prev">
                    <svg width="58" height="59" viewBox="0 0 58 59" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M34.7147 14.9287L38.0435 18.3564L27.106 29.6191L38.0435 40.8818L34.7147 44.3096L20.4483 29.6191L34.7147 14.9287Z"
                              fill="#103262"/>
                    </svg>
                </div>
                <div class="swiper-button-next sem-image-slider-next">
                    <svg width="58" height="59" viewBox="0 0 58 59" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M22.3503 44.0713L19.0215 40.6436L29.959 29.3809L19.0215 18.1182L22.3503 14.6904L36.6166 29.3809L22.3503 44.0713Z"
                              fill="#103262"/>
                    </svg>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>
