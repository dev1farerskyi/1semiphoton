<?php
/**
 * Contact Us Block
 *
 */


$contact_us_info = get_field('contact_us_info', 'option');
$contact_form = get_field('contact_form', 'option');
?>

<div class="sem-page">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <div class="sem-contact__info">
                    <?php if ( ! empty( $contact_us_info ) ): ?>
                        <h5 class="sem-contact__subtitle"><?php echo $contact_us_info['subtitle']; ?></h5>
                    <?php endif; ?>
                    <?php if ( ! empty( $contact_us_info ) ): ?>
                        <h3 class="sem-contact__title"><?php echo $contact_us_info['title']; ?></h3>
                    <?php endif; ?>
                    <?php if ( ! empty( $contact_us_info ) ): ?>
                        <div class="sem-contact__info-text"><?php echo $contact_us_info['text']; ?></div>
                    <?php endif; ?>
                    <?php if ( ! empty( $contact_us_info ) ): ?>
                        <a href="https://www.google.com/maps/search/?api=1&query=<?php echo $contact_us_info['address']; ?>" target="_blank" rel="nofollow" class="sem-contact-line text_blue mb-10">
                        <!-- <div class="sem-contact-line text_blue mb-10"> -->
                                <span class="icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18"
                                         fill="none">
                                        <path d="M9 1.5C6.0975 1.5 3.75 3.8475 3.75 6.75C3.75 10.6875 9 16.5 9 16.5C9 16.5 14.25 10.6875 14.25 6.75C14.25 3.8475 11.9025 1.5 9 1.5ZM9 8.625C8.50272 8.625 8.02581 8.42745 7.67418 8.07582C7.32254 7.72419 7.125 7.24728 7.125 6.75C7.125 6.25272 7.32254 5.7758 7.67418 5.42417C8.02581 5.07254 8.50272 4.875 9 4.875C9.49728 4.875 9.9742 5.07254 10.3258 5.42417C10.6775 5.7758 10.875 6.25272 10.875 6.75C10.875 7.24728 10.6775 7.72419 10.3258 8.07582C9.9742 8.42745 9.49728 8.625 9 8.625Z"
                                              fill="#F8C323"/>
                                    </svg>
                                </span>
                            <?php echo $contact_us_info['address']; ?>
                        <!-- </div> -->
                        </a>
                    <?php endif; ?>
                    <?php if ( ! empty( $contact_us_info ) ): ?>
                        <a href="mailto:<?php echo $contact_us_info['email']; ?>" class="sem-contact-line mb-10">
                                    <span class="icon">
                                        <svg width="18" height="19" viewBox="0 0 18 19" fill="none"
                                             xmlns="http://www.w3.org/2000/svg">
                                            <path d="M1.45913 7.24031C1.06763 7.10981 1.06388 6.89906 1.46663 6.76481L15.7819 1.99331C16.1786 1.86131 16.4059 2.08331 16.2949 2.47181L12.2044 16.7863C12.0919 17.1831 11.8631 17.1966 11.6951 16.8201L8.99963 10.7541L13.4996 4.75406L7.49963 9.25406L1.45913 7.24031Z"
                                                  fill="#F8C323"/>
                                        </svg>
                                    </span>
                            <?php echo $contact_us_info['email']; ?>
                        </a>
                    <?php endif; ?>
                    <?php if ( ! empty( $contact_us_info ) ): ?>
                    <a href="tel:+<?php echo $contact_us_info['phone']; ?>" class="sem-contact-line mb-15">
                                <span class="icon">
                                    <svg width="18" height="19" viewBox="0 0 18 19" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path d="M4.965 8.34641C6.045 10.4689 7.785 12.2014 9.9075 13.2889L11.5575 11.6389C11.76 11.4364 12.06 11.3689 12.3225 11.4589C13.1625 11.7364 14.07 11.8864 15 11.8864C15.4125 11.8864 15.75 12.2239 15.75 12.6364V15.2539C15.75 15.6664 15.4125 16.0039 15 16.0039C7.9575 16.0039 2.25 10.2964 2.25 3.25391C2.25 2.84141 2.5875 2.50391 3 2.50391H5.625C6.0375 2.50391 6.375 2.84141 6.375 3.25391C6.375 4.19141 6.525 5.09141 6.8025 5.93141C6.885 6.19391 6.825 6.48641 6.615 6.69641L4.965 8.34641Z"
                                              fill="#F8C323"/>
                                    </svg>
                                </span>
                        +<?php echo $contact_us_info['phone']; ?>
                    </a>
                    <?php endif; ?>
                    <div class="sem-contact__map">
                        <div class="gmap_canvas">
                            <iframe class="gmap_iframe" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
                                    src="https://maps.google.com/maps?width=413&amp;height=176&amp;hl=en&amp;q= 2076 - 16th Avenue, Ste. A San Francisco, CA 94116, USA&amp;t=&amp;z=14&amp;ie=UTF8&amp;iwloc=B&amp;output=embed"></iframe>
                            <a href="https://www.gachacute.com/"></a></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8">
                <?php if ( ! empty( $contact_form ) ): ?>
                    <?php echo do_shortcode('[gravityform id="' . $contact_form['id'] . '" title="false" description="false" ajax="true"]'); ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

