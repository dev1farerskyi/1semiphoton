<?php
/**
 * Card Info Block
 */

$title = get_sub_field('title');
$text = get_sub_field('text');
$link = get_sub_field('link');
$image = get_sub_field('image');
$image_position = get_sub_field('image_position');
$bg_color = get_sub_field('bg_color');
$indents = get_sub_field('indents') ? 'indents' : '';
?>

<div id="sem-block-<?php echo get_row_index(); ?>" class="sem-card-info card-full-image <?php echo $image_position;?> <?php echo $bg_color; ?> <?php echo $indents; ?>">
    <div class="sem-card-info__wrap">
        <div class="container">
            <div class="row align-items-center <?php echo $image_position === 'image_right' ? 'flex-md-row-reverse image_right flex-column' : ''; ?>">
                <div class="col-lg-6">
                    <div class="sem-card-info__image">
                        <img src="<?php echo esc_url( $image['url'] ); ?>" alt="image">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="sem-card-info__content">
                        <h4 class="sem-card-info__title"><?php echo wp_kses_post( $title ); ?></h4>
                        <div class="sem-card-info__text">
                            <?php echo wpautop( $text ); ?>
                        </div>
                        <?php semiphoton_btn($link, 'sem-btn sem-btn_main'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>