<?php
/**
 *  Main Title Block
 */
$main_title = get_sub_field('main_title');
$style = get_sub_field('style');
$tag = $style == 'big' ? 'h1' : 'h2';

?>

<div id="sem-block-<?php echo get_row_index(); ?>" class="sem-main-title">
    <div class="sem-main-title__wrap">
        <div class="container">
            <<?php echo $tag; ?> class="sem-page__title <?php echo $style; ?>"><?php echo wp_kses_post($main_title); ?></<?php echo $tag; ?>>
        </div>
    </div>
</div>

