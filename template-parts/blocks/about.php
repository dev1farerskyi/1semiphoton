<?php
/**
 * About Block
 */

$style = get_sub_field('style');
$title = get_sub_field('title');
$subtitle = get_sub_field('subtitle');
$text = get_sub_field('text');
$link = get_sub_field('link');
$image = get_sub_field('image');
$bg_color = get_sub_field('bg_color');
$indents = get_sub_field('indents') ? 'indents' : '';
?>

<div id="sem-block-<?php echo get_row_index(); ?>"
     class="<?php echo $style === 's2' ? 'sem-mission' : 'sem-card-about'; ?> <?php echo $bg_color; ?> <?php echo $indents; ?>">
    <div class="container">
        <div class="row align-items-lg-center flex-md-row flex-column-reverse">
            <?php if ($style === 's2') : ?>
                <div class="col-md-6">
                    <div class="sem-mission__content">
                        <div class="sem-mission__text">
                            <?php echo wpautop($text); ?>
                        </div>
                        <h2 class="sem-mission__title"><?php echo wp_kses_post($title); ?></h2>
                        <?php semiphoton_btn($link, 'sem-btn sem-btn_main'); ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="sem-mission__image">
                        <img src="<?php echo esc_url($image['url']); ?>" alt="image">
                    </div>
                </div>
            <?php else : ?>
                <div class="col-md-6">
                    <div class="sem-card-about__content">
                        <h5 class="sem-card-about__subtitle"><?php echo esc_html($subtitle); ?></h5>
                        <h2 class="sem-card-about__title"><?php echo wp_kses_post($title); ?></h2>
                        <div class="sem-card-about__text">
                            <?php echo wpautop($text); ?>
                        </div>
                        <?php semiphoton_btn($link, 'sem-btn sem-btn_main'); ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="sem-card-about__image">
                        <img src="<?php echo esc_url($image['url']); ?>" alt="image">
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>