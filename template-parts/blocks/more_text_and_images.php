<?php
/**
 *  More Text And Images Block
 */
$title = get_sub_field('title');
$link = get_sub_field('link');
$text = get_sub_field('text');
$images = get_sub_field('gallery_images');
$count_images = count($images);
?>

<div id="sem-block-<?php echo get_row_index(); ?>" class="sem-card-info">
    <div class="sem-card-info__wrap">
        <div class="container">
            <div class="row flex-lg-row flex-column-reverse align-items-center">
                <div class="col-xl-<?php echo $count_images === 3 ? '4' : '8'; ?> col-lg-6">
                    <div class="sem-card-info__content">
                        <?php if ( ! empty( $title ) ): ?>
                            <h4 class="sem-card-info__title"><?php echo wp_kses_post($title); ?></h4>
                        <?php endif; ?>
                        <?php if ( ! empty( $text ) ): ?>
                            <div class="sem-card-info__text">
                                <?php echo wpautop($text); ?>
                            </div>
                        <?php endif; ?>
                        <?php semiphoton_btn($link, 'sem-btn sem-btn_main'); ?>
                    </div>
                </div>
                <div class="col-xl-<?php echo $count_images === 3 ? '8' : '4'; ?> col-lg-6">
                    <?php if ($images): ?>
                        <div class="sem-card-info__gallery <?php echo $count_images === 3 ? 'row' : ''; ?>">
                            <?php foreach ($images as $key => $image): ?>
                                <?php if ( $count_images === 3 && $key === 0 ) : ?>
                                    <div class="col-lg-6">
                                        <div class="sem-card-info__image">
                                            <img src="<?php echo $image['sizes']['large']; ?>" alt="image">
                                        </div>
                                    </div><div class="col-lg-6">
                                <?php else : ?>
                                    <div class="sem-card-info__image">
                                        <img src="<?php echo $image['sizes']['large']; ?>" alt="image">
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
