<?php
/**
 * Hero Two Culumn Block
 */

$image = get_sub_field('image');
$title = get_sub_field('title');
$text = get_sub_field('text');
?>

<div id="sem-block-<?php echo get_row_index(); ?>" class="sem-article">
    <div class="container">
        <div class="sem-article__head">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <?php if ( ! empty( $title ) ): ?>
                        <h2 class="sem-article__title mb-40 mb-lg-0"><?php echo wp_kses_post($title); ?></h2>
                    <?php endif; ?>
                </div>
                <div class="col-lg-6">
                    <?php if ( ! empty( $image ) ): ?>
                        <div class="sem-article__main-image">
                            <img src="<?php echo esc_url($image['url']); ?>" alt="image">
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <?php if ( ! empty( $text ) ): ?>
            <div class="sem-article__content">
                <?php echo wpautop($text); ?>
            </div>
        <?php endif; ?>
    </div>
</div>
