<?php
/**
 * News Block
 */

$title = get_sub_field('title');
$link = get_sub_field('link');
$posts = get_sub_field('posts');

$query_args = array(
    'post_type' => 'post',
    'post_status' => 'publish',
    'posts_per_page' => -1,
    'post__in' => $posts
);

$posts_query = new WP_Query($query_args);

if ( $posts_query->have_posts() ) : ?>
    <div id="sem-block-<?php echo get_row_index(); ?>" class="sem-news-block bg_gray">
        <div class="container">
            <?php if ( ! empty( $title ) ) : ?>
                <h5 class="sem-news-block__subtitle"><?php echo esc_html( $title ); ?></h5>
            <?php endif; ?>

            <div class="row">
                <?php while ( $posts_query->have_posts() ) : $posts_query->the_post();
                    get_template_part('template-parts/news/content');
                endwhile; wp_reset_postdata(); ?>
            </div>

            <div class="d-flex justify-content-center">
                <?php semiphoton_btn($link, 'sem-btn sem-btn_main'); ?>
            </div>
        </div>
    </div>
<?php endif;
