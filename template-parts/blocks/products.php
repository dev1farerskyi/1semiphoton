<?php
/**
 * Products Block
 */

$items = get_sub_field('items');

if ( ! empty( $items ) ) : ?>
    <div id="sem-block-<?php echo get_row_index(); ?>" class="sem-products bg_gray">
        <div class="container">
            <div class="row justify-content-center">
                <?php foreach ( $items as $item ) : ?>
                    <div class="col-lg-4 col-sm-6">
                        <a href="<?php echo esc_url( $item['link']['url'] ); ?>" class="sem-products__item">
                            <div class="sem-products__item-image">
                                <img src="<?php echo esc_url( $item['image']['url'] ); ?>" alt="image">
                                <h5 class="sem-products__item-title"><?php echo esc_html( $item['title'] ); ?></h5>
                            </div>
                            <div class="sem-products__item-bottom">
                                <div class="sem-link link_arrow mb-0"><?php echo esc_html( $item['link']['title'] ); ?>
                                    <span class="icon">
                                        <svg width="21" height="8" viewBox="0 0 21 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M20.3536 4.35355C20.5488 4.15829 20.5488 3.84171 20.3536 3.64645L17.1716 0.464466C16.9763 0.269204 16.6597 0.269204 16.4645 0.464466C16.2692 0.659728 16.2692 0.976311 16.4645 1.17157L19.2929 4L16.4645 6.82843C16.2692 7.02369 16.2692 7.34027 16.4645 7.53553C16.6597 7.7308 16.9763 7.7308 17.1716 7.53553L20.3536 4.35355ZM0 4.5H20V3.5H0V4.5Z" fill="#F8C323" />
                                        </svg>
                                    </span>
                                </div>
                            </div>
                        </a>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
<?php endif;
