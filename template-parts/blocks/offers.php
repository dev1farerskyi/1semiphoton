<?php
/**
 * Offers Block
 */

$title = get_sub_field('title');
$list = get_sub_field('list');
$image = get_sub_field('image');
?>

<div id="sem-block-<?php echo get_row_index(); ?>" class="sem-customers">
    <div class="container">
        <div class="d-flex sem-customers__wrap">
            <?php if ( ! empty( $image ) ) : ?>
                <div class="sem-customers__image">
                    <img src="<?php echo esc_url( $image['url'] ); ?>" alt="image">
                </div>
            <?php endif; ?>

            <div class="sem-customers__content">
                <?php if ( ! empty( $title ) ) : ?>
                    <h3 class="sem-customers__title"><?php echo wp_kses_post( $title ); ?></h3>
                <?php endif; ?>

                <?php if ( ! empty( $list ) ) :
                    $list = explode(PHP_EOL, $list); ?>
                    <div class="d-none d-lg-block sem-customers__info">
                        <ul>
                            <?php foreach ( $list as $item ) : ?>
                                <li><?php echo trim( $item ); ?></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                <?php endif; ?>
            </div>
        </div>

        <?php if ( ! empty( $list ) ) : ?>
            <div class="d-block d-lg-none sem-customers__info">
                <ul>
                    <?php foreach ( $list as $item ) : ?>
                        <li><?php echo trim( $item ); ?></li>
                    <?php endforeach; ?>
                </ul>
            </div>
        <?php endif; ?>
    </div>
</div>
