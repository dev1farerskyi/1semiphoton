<?php
/**
 * Gallery Block
 */

$images = get_sub_field('gallery_images');
$gallery_type = get_sub_field('gallery_type');
$text = get_sub_field('text');
$link = get_sub_field('link');
$position_content = 'pos-' . get_sub_field('position_content');
?>

<div id="sem-block-<?php echo get_row_index(); ?>" class="sem-gallery <?php echo $position_content; ?>">
    <div class="container">
        <?php if ( ! empty( $images ) ) : ?>
            <?php if ( $gallery_type === 'slider' ) : ?>
                <div class="sem-about__slider">
                    <div class="swiper sem-image-slider">
                        <div class="swiper-wrapper">
                            <?php foreach ($images as $item): ?>
                                <div class="swiper-slide">
                                    <div class="sem-about__slider-item">
                                        <img src="<?php echo esc_url($item['url']); ?>" alt="image">
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>

                    <div class="swiper-button-prev sem-image-slider-prev">
                        <svg width="58" height="59" viewBox="0 0 58 59" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M34.7147 14.9287L38.0435 18.3564L27.106 29.6191L38.0435 40.8818L34.7147 44.3096L20.4483 29.6191L34.7147 14.9287Z"
                                  fill="#103262"/>
                        </svg>
                    </div>
                    <div class="swiper-button-next sem-image-slider-next">
                        <svg width="58" height="59" viewBox="0 0 58 59" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M22.3503 44.0713L19.0215 40.6436L29.959 29.3809L19.0215 18.1182L22.3503 14.6904L36.6166 29.3809L22.3503 44.0713Z"
                                  fill="#103262"/>
                        </svg>
                    </div>
                </div>
            <?php else : ?>
                <div class="sem-gallery__wrap">
                    <?php foreach ($images as $image): ?>
                        <div class="col-lr-3">
                            <a href="<?php echo $image['sizes']['large']; ?>" class="sem-gallery__item"><img src="<?php echo $image['sizes']['large']; ?>" alt=""></a>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        <?php endif; ?>

        <?php if ( ! empty( $title ) || ! empty( $link ) ): ?>
            <div class="sem-connection mb-lg-150 mb-md-100 mb-50">
                <div class="sem-connection__content">
                    <?php if ( ! empty( $title ) ): ?>
                        <h3><?php echo wpautop($text); ?></h3>
                    <?php endif; ?>

                    <?php semiphoton_btn($link, 'sem-btn sem-btn_main'); ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div