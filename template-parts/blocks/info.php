<?php
/**
 *  Info Block
 */

$title = get_sub_field('title');
$text = get_sub_field('text');
$list = get_sub_field('list');

?>

<div id="sem-block-<?php echo get_row_index(); ?>" class="sem-customers design_customers">
    <div class="container">
        <div class="sem-customers__wrap">
            <div class="sem-customers__content">
                <div class="row">
                    <div class="col-lg-6">
                        <?php if ( ! empty( $title ) ): ?>
                            <h3 class="sem-customers__title">
                                <?php echo wp_kses_post($title); ?>
                            </h3>
                        <?php endif; ?>

                        <?php if ( ! empty( $text ) ): ?>
                            <div class="sem-customers__text">
                                <?php echo wpautop($text); ?>
                            </div>
                        <?php endif; ?>
                    </div>

                    <div class="col-lg-6">
                        <?php if ( ! empty( $list ) ): ?>
                            <div class="d-none d-lg-block sem-customers__info sem-customers__info-full">
                                <?php echo wpautop($list); ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
