<?php
/**
 * Image Block
 */

$image = get_sub_field('image');

if ( ! empty( $image ) ) : ?>
    <div id="sem-block-<?php echo get_row_index(); ?>" class="sem-image">
        <div class="container">
            <img src="<?php echo esc_url( $image['url'] ); ?>" alt="image">
        </div>
    </div>
<?php endif;
