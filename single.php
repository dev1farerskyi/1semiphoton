<?php
/**
 * Single post template
 */

get_header();

if ( have_posts() ) :
    while ( have_posts() ) : the_post(); ?>

        <div class="sem-article">
            <div class="container">
                <div class="sem-article__head">
                    <div class="row align-items-center">
                        <div class="col-lg-<?php echo has_post_thumbnail() ? '6' : '12'; ?>">
                            <?php the_title('<h2 class="sem-article__title mb-40 mb-lg-0">', '</h2>'); ?>
                        </div>

                        <?php if ( has_post_thumbnail() ) : ?>
                            <div class="col-lg-6">
                                <div class="sem-article__main-image">
                                    <?php the_post_thumbnail('large'); ?>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="sem-article__content">
                    <?php the_content(); ?>
                </div>
            </div>
        </div>

        <?php get_template_part('template-parts/news/info'); ?>

    <?php
    endwhile;
endif;

get_footer();