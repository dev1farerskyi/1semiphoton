<?php

/**
 * ACF Options page support
 */
if ( function_exists('acf_add_options_page') ) {
	acf_add_options_page();	
}

/**
 * Get link
 *
 * @param $link_arr
 * @param string $class
 */
function semiphoton_btn($link_arr, string $class = '') {
    $class_btn = array();

    if ( ! empty( $class ) ) {
        $class_btn[] = $class;
    }

    if ( ! empty( $link_arr ) ) {
        $target = ! empty( $link_arr['target'] ) && $link_arr['target'] === '_blank' ? 'target="_blank"' : '';
        echo '<a class="' . esc_attr( implode(' ', $class_btn ) ) . '" href="' . esc_url( $link_arr['url'] ) . '" ' . $target . '>' . esc_attr( $link_arr['title'] ) . '</a>';
    }
}

/**
 * Ajax load more.
 */
function semiphoton_load_more(){
    $args = array(
        'post_type' => 'post',
        'post_status' => 'publish',
        'posts_per_page' => get_option('posts_per_page'),
        'paged' => $_POST['page']
    );

    $news_query = new WP_Query($args);

    ob_start();

    if( $news_query->have_posts() ) :
        while ( $news_query->have_posts() ) : $news_query->the_post();
            get_template_part('template-parts/news/content');
        endwhile; wp_reset_postdata();
    endif;

    $output['posts'] = ob_get_clean();
    $output['max_page'] = $news_query->max_num_pages;

    echo json_encode( $output );

    wp_die();
}
add_action('wp_ajax_blog_load_more', 'semiphoton_load_more');
add_action('wp_ajax_nopriv_blog_load_more', 'semiphoton_load_more');



function semiphoton_vimeo_omebed( $html, $url, $args ) {
    
    $doc = new DOMDocument();
    $doc->loadHTML( $html, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD );
    $tags = $doc->getElementsByTagName( 'iframe' );

    foreach ( $tags as $tag ) {
        $iframe_src = $tag->attributes->getNamedItem('src')->value;

        if ( false !== strpos( $iframe_src, 'youtube.com' ) ) {

            $e = parse_url($url);
            parse_str($e['query'], $query);

            $query_args = array(
                'autoplay' => 1,
                'loop' => 1,
                'mute' => 1,
                'autohide' => 1,
                'controls' => 0,
                'showinfo' => 0,
                'rel' => 0,
                'wmode' => 'transparent',
            );

            if ( isset( $query['v'] ) ) {
                $query_args['playlist'] = $query['v'];
            }

            $url = add_query_arg( $query_args, $iframe_src );
        }

        if ( false !== strpos( $iframe_src, 'vimeo.com' ) ) {
            $url = add_query_arg( array(
                'autoplay' => 1,
                'loop' => 1,
                'muted' => 1,
                'background' => 1,
                'badge' => 0,
                'controls' => 0,
                'byline' => 0,
                'portrait' => 0,
                'title' => 0,
            ), $iframe_src );
        }

        $tag->setAttribute( 'src', $url );

        $html = $doc->saveHTML();
    }

    return $html;
}
add_filter( 'oembed_result', 'semiphoton_vimeo_omebed', 10, 3);
