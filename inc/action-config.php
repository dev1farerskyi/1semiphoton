<?php


/**
 * Include google fonts.
 *
 * @return string
 */
function semiphoton_fonts_url() {
    $fonts = array(
        'Rubik:wght@300;400;500;600'
    );
    $font_url = '';
    /*
    Translators: If there are characters in your language that are not supported
    by chosen font(s), translate this to 'off'. Do not translate into your own language.
     */
    if ( 'off' !== _x( 'on', 'Google font: on or off', SP_PREFIX ) ) {
        $font_url = add_query_arg( 'family', ( implode( '&', $fonts ) . "&subset=latin,latin-ext" ), "//fonts.googleapis.com/css2" );
    }
    return $font_url;
}

/**
 * Enqueue scripts.
 */
function semiphoton_enqueue_scripts() {
    wp_enqueue_style( 'main-fonts', semiphoton_fonts_url(), array() );

    wp_enqueue_style( 'magnific', '//cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css' );
    wp_enqueue_style( SP_PREFIX . '-style', get_stylesheet_directory_uri() . '/assets/css/style.css' );

    wp_enqueue_script('masonry');
    // wp_enqueue_script( 'iframe_api', '//www.youtube.com/iframe_api', array('jquery'), '1.0.0', true );
    wp_enqueue_script( 'magnific', '//cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.js', array('jquery'), '1.0.0', true );
    wp_enqueue_script( SP_PREFIX . '-script', get_stylesheet_directory_uri() . '/assets/js/script.js', array('jquery'), '1.0.0', true );

    wp_localize_script( SP_PREFIX . '-script', SP_PREFIX . '_object', array(
        'ajax_url' => admin_url( 'admin-ajax.php' ),
        'site_url' => site_url('/')
    ) );

}
add_action( 'wp_enqueue_scripts', 'semiphoton_enqueue_scripts' );
