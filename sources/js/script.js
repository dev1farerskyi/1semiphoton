import './app/gutenberg';
import Swiper from 'swiper/swiper-bundle';
// import {gsap} from "./app/gsap/gsap";
// import {ScrollTrigger} from "./app/gsap/ScrollTrigger";
import 'youtube-background';
import {isEven, isjQuery, Coordinates, videoResize} from "./app/functions";

// gsap.registerPlugin(ScrollTrigger);


(function ($) {

    // Video bg
    if ( $('[data-vbg]').length ) {
        $('[data-vbg]').youtube_background({
            'load-background': false,
        });
    }

    // Header
    $('.sem-header__burger').on('click', function () {
        if($(this).hasClass('active')) {
            $(this).removeClass('active');
            $('.sem-header').removeClass('show');
            $('body').removeClass('modal-open');
        } else {
            $(this).addClass('active');
            $('.sem-header').addClass('show');
            $('body').addClass('modal-open');
        }
    });

    $('.menu-item-has-children>a').on('click', function (event) {
        event.preventDefault()
        let parent = $(this).parent();
        parent.toggleClass('show_menu');
    })

    // Slider
    var imageSlider = new Swiper(".sem-image-slider", {
        slidesPerView: 2,
        spaceBetween: 10,
        loop: true,
        navigation: {
            nextEl: '.sem-image-slider-next',
            prevEl: '.sem-image-slider-prev',
        },
        breakpoints: {
            600: {
                slidesPerView: 3,
                spaceBetween: 20,
            },
            991: {
                slidesPerView: 4,
            }
        }
    });

    // Gallery
    if ( $('.wp-block-gallery').length ) {
        var _gallery = $('.wp-block-gallery').imagesLoaded( function() {
            _gallery.masonry({
                itemSelector: '.wp-block-image',
                percentPosition: true
            }).magnificPopup({
                delegate: 'a',
                type: 'image',
                gallery: {
                    enabled: true,
                },
            });
        });
    }

    $('.sem-gallery__item').magnificPopup({
        // delegate: 'a',
        type: 'image',
        gallery: {
            enabled: true,
        },
    });

    // Load more posts
    var canBeLoaded = true;
    var current_page = 2;

    $('.js-load-btn').on('click', function (e) {
        e.preventDefault();

        var _this = $(this),
            _wrapper = _this.closest('.js-load-wrapper');

        $.ajax({
            url: semiphoton_object.ajax_url,
            data: {
                'action': 'blog_load_more',
                'page': current_page
            },
            dataType: 'json',
            type: 'POST',
            beforeSend: function (xhr) {
                canBeLoaded = false;
                _this.text('Loading...');
            },
            success: function (response) {

                var $items = $.parseHTML(response.posts);

                _wrapper.find('.js-load-container').append($items);

                if (current_page == response.max_page) {
                    _this.parent().remove();
                } else {
                    current_page++;
                    canBeLoaded = true;
                    _this.text('Load More');
                }
            }
        });
    });


    // loader
    if ( $('.sem-loader').length ) {
        setTimeout(function(){
            $('.sem-loader').addClass('active');
        }, 2000);
    }


})(jQuery);