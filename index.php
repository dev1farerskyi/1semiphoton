<?php
/**
 * The main template file
 */
global $wp_query;

$news_image = get_field('news_image', 'option');
$news_title = get_field('news_title', 'option');
$news_title = ! empty( $news_title ) ? $news_title : get_the_title( get_option('page_for_posts', true) );

get_header();
?>

    <?php if ( ! empty( $news_image ) ) : ?>
        <div class="sem-hero">
            <div class="container">
                <div class="sem-hero__image">
                    <img src="<?php echo esc_url( $news_image['url'] ); ?>" alt="<?php echo $news_image['alt'] ?>">
                </div>
            </div>
        </div>
    <?php endif; ?>

    <div class="sem-page">
        <div class="container">
            <h1 class="sem-page__title"><?php echo $news_title; ?></h1>
            
            <div class="js-load-wrapper">
                <div class="row js-load-container">
                    <?php if ( have_posts() ) : ?>

                        <?php while ( have_posts() ) : the_post();
                            get_template_part('template-parts/news/content');
                        endwhile; ?>

                    <?php else : ?>
                        <div class="sem-article__content">
                            <p><?php esc_html_e('Sorry, no news matched your criteria.', SP_PREFIX); ?></p>
                            <?php get_search_form(); ?>
                        </div>
                    <?php endif; ?>
                </div>

                <?php if ( $wp_query->max_num_pages > 1 ): ?>
                    <div class="d-flex justify-content-center">
                        <a href="#" class="sem-btn sem-btn_main js-load-btn"><?php esc_html_e('Load More', SP_PREFIX); ?></a>
                    </div>
                <?php endif ?>
            </div>
        </div>
    </div>

<?php
get_footer();
