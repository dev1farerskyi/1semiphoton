<?php
/**
 * Header template.
 *
 * @package semiphoton
 * @since 1.0.0
 *
 */

$header_logo = get_field('header_logo', 'option');
$show_loader = true;
if ( is_front_page() ) {
	if ( isset( $_COOKIE['show_loader'] ) && $_COOKIE['show_loader'] == 'no' ) {
		$show_loader = false;
	} else {
		setcookie('show_loader', 'no', time() + 60 * 60 * 24 * 30, '/');
	}
}
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="apple-mobile-web-app-capable" content="yes"/>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
	<meta name="format-detection" content="telephone=no"/>
	<?php wp_head(); ?>

	<script type="text/javascript">
		(function (d, t) {
			var pp = d.createElement(t), s = d.getElementsByTagName(t)[0];
			pp.src = '//app.pageproofer.com/embed/f63d8303-166b-566b-8d76-05f058b16eb6';
			pp.type = 'text/javascript';
			pp.async = true;
			s.parentNode.insertBefore(pp, s);
		})(document, 'script');
	</script>
</head>
<body <?php body_class(); ?>>

<header class="sem-header">
	<?php if ( $show_loader && is_front_page() ): ?>
		<div class="sem-loader">
			<div class="sem-loader__gray">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/logo-gray.png">
			</div>
			<div class="sem-loader__color">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/logo-color.png">
			</div>
		</div>
	<?php endif ?>

	<div class="container">
		<div class="d-flex sem-header__wrap">
			<a href="<?php echo home_url('/'); ?>" class="sem-header__logo">
				<img src="<?php echo esc_url( $header_logo['url'] ); ?>" alt="logo">
			</a>

			<button class="sem-header__burger d-lg-none d-block">
				<span></span>
				<span></span>
				<span></span>
			</button>

			<nav class="sem-header__nav">
				<?php wp_nav_menu(
					array(
						'container' => '',
						'items_wrap' => '<ul class="sem-header__menu">%3$s</ul>',
						'theme_location' => 'main-menu',
						'depth' => 2,
						'fallback_cb' => '__return_empty_string',
					)
				); ?>

				<div class="gtranslate_wrapper"></div>
			</nav>
		</div>
	</div>
</header>

<main class="sem-main-wrapper">
