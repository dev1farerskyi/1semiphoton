<?php
/**
 * The file includes necessary functions for theme.
 *
 * @package semiphoton
 * @since 1.0
 */

if (!defined('SP_PREFIX')) define('SP_PREFIX', 'semiphoton');

require_once get_theme_file_path( '/inc/custom-post-type.php' );
require_once get_theme_file_path( '/inc/action-config.php' );
require_once get_theme_file_path( '/inc/helper-functions.php' );

// Register ACF Gravity Forms field
add_action( 'init', function () {
    if ( class_exists( 'ACF' ) ) {
        require_once get_stylesheet_directory() . '/inc/class-acf-field-gravity-v5.php';
    }
} );

function semiphoton_after_theme_setup() {

    register_nav_menus(
        array(
            'main-menu' => esc_html__( 'Main menu', SP_PREFIX ),
            'footer-menu-1' => esc_html__( 'Footer menu 1', SP_PREFIX ),
            'footer-menu-2' => esc_html__( 'Footer menu 2', SP_PREFIX ),
            'footer-menu-3' => esc_html__( 'Footer menu 3', SP_PREFIX ),
        )
    );

    add_theme_support( 'custom-header' );
    add_theme_support( 'automatic-feed-links' );
    add_theme_support( 'post-thumbnails' );
    add_theme_support( 'title-tag' );
}
add_action( 'after_setup_theme', 'semiphoton_after_theme_setup' );
